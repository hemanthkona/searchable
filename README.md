# Searchable

## Serve: 
- Install node dependencires
  
  > npm install

### React App:
 > nx serve e-bay-search --open 

 or 

 > npx nx serve e-bay-search --open

 - Open: http://localhost:4200



## Design of the API request/response schema.
> Request: libs/api-interfaces/src/lib/Request.ts
> Response: libs/api-interfaces/src/lib/Response.ts

### Request
```ts
interface QueryProduct {
  title?: string;
  condition?: Array<string>;
  price?: number;
  min_price?: number; // converted to cents
  max_price?: number; // converted to cents
  location_string?: string;
  location?: Location;
  category?: string;
  _created_date?: Date;
  _last_updated_date?: Date;
}

interface RequestOptions {
  sort: string;
  skip: number;
  limit: number;
  page: number;
  no_of_items_per_page: number;
}

interface Request {
  request_id: string;
  collection?: string;
  query: QueryProduct;
  options?: RequestOptions;
}
```
```
Ex Request:
Post http://domain.com/api
request: {
  request_id: "generate001",
  collection: "products",
  query: {
    title: "apple watch",
    min_price: 32525, // in cents 
    max_price: 56575, // in cents
  }
}
```

- I have used MongoDb(NoSql) terminology Ex: Collection == Table, Document == Record 
- For discussion purpose. I'm using "products" as a collection/table
- The request to backend is a POST request because GET request is not considered safe and we have a restriction on length of the url, so we may not be able to fit a long query 
- Every request obj will have a request_id: which is unique id to identify each request. Benefits of having request_id is we can refer to the request obj if any particular user or third party apps having issues. If the request obj and response obj is stored in the databse we can debug the issue quickly.
- collection refer to a database table/collection, we can specify a collection/table that we want to query
- query field is where define the where clause to filter the records/documents in the specified collection/table
- options field is where we can specify 
  - limit: How many products we want retrieve
  - sort: which field we want to use to sort: for example: _created_date
  - skip: skip and limit can be used in combination to do the pagination


## Possible errors and edge cases

- Currently I'm using Flexbox to ararnge the elements on the page, if the user is using older browser which doesn't support flexbox we have to write or use a library which handles older browsers using "Box model" instead of "Flex box" 
- Multiple duplicate backend calls for same query, we need to check if the query is changed before sending a backend query other we might do the same query multiple times 
- Debounce search input so it doesn;t trigget state update on every single keychange
- Currently we can still use the filter, search UI while the request for getting products is in progress. We should Block the ui for searching, filtering while request in progress. Otherwise the use might see a mismatched results 
- Setting Query params are hardcoded for each field, dynamic settings query params by looping through the queryParam entries is a better approach 
- Form validation and error messages, showing proper form validation and giving the feedback to the user 
- Showing outdated data to users, if we are not using websockets to push new updates to front-end


## Testing

- Before coming to testing, we can fix or find most of the human errors using a code linting tool like eslint.
- Typescript also helps in defining the types/structre of the obj which reduces the spelling mistakes or undefined errors

### Unit Testing
- Unit testing is the most basic form of testing. For example we can write a unit test to test the filter function by sending in the query obj and testing whether we get the expexted results
```ts
// test filter function
import { filter } from 'filter.ts'

const dummyData: Products[] = [{
  title: "Apple Watch"
},
{
  title: "Android mobile"
}];

const query = {
  title: "apple watch"
}

const filter = (data, query) => {
  // filter data using query
  return result;
}

describe('Filter', () => {
  test("Should return Apple watch", () => {
    expect(filter(dummyData,query)).toBe({title: "Apple Watch"});
  });
})

```

### e2e (End-to-end) Testing
- Test the UI using Cypress or similar UI testing tool
- Test Case1:  Test whether all elements like "Search", "Filter", "Procucts" are present in the dom
- Test Case2: Test if no products are found the "Procucts component" should not be found in the dom
 
### API Testing
- Testing the api response, whether we are getting the expcted result. 
- We can use tools like Postman, Mocha for rest api testing


## Pagination and sorting of results.

### Using backend only
- Sort First after querying: We have sort the results first before we can use limit or skip
- We can use the skip, limit features of the database to do pagination and send data as needed to the front-end.
- When we first run a query, we can query the database and filter the data using the query and then return only the no.of items = limit value
For ex: 
```json
{
  sort: {
    _created_date: -1 
  },
  limit: 10,
  skip: 0,
  page: 1,
}
or 
/products?limit=10&skip=0 if we are using queryparams
```
- For the second time if the use clicked on page 2 then we set the skip value to 10 items and limit = 10
For ex: 
```json
{
  limit: 10,
  skip: 10,
  page: 2
}
```
- If user clicks on page 10 then we can skip first 9 pages by setting the skip = limit * 9 = 90
For ex: 
```json
{
  limit: 10,
  skip: 90,
  page: 10
}
```

### Using backend and front-end
- In the first method using backend only we did lot of queyring back and forth from server to client. It is not very performant or use friendly. 
- Second option is to use combination of backend and front-end and do pagination
- We will still use skip, limit, sort features as we did in the backend only method but we will do pagination in the front-end as well
For ex: 
- no_of_item_to_show_on_each_page = 10
Then we query the databse and get 100 items on every request. 
```json
{
  sort: {
    _created_date: -1 
  },
  limit: 100,
  skip: 0,
  no_of_items_per_page: 10
} 
```
- We got 100 items on the request but we only show 10 items to the end use. And when he clicks next page we already have the next page data so we just show that from locally stored data
- Once we reach page 10 we dont have data to show so then we query databse again for next 100 items and so on
- We can do Intelligent loading data, by prefetching data before we reach the page 10


## Loading page faster
- Use caching techniques using service worker or localstorage to speed the search for a user
- We can use CDN (Content Deliver Networks) to load data faster because the data is distributed to the nearby servers which will give the users fast access to data
- Send the text data first and then send the images later so that the use can see some data loading quickly
- We can do prefetching data in the background by prefetching the frequently searched item based on the users current location and frequently searched keywords



## Folder Structure: 

### React app
  > apps/e-bay-search/src/app/app.tsx
### Schema
  > libs/api-interfaces/src/lib
### Dummy Data
  > libs/data/src/lib

## Assumptions: 
  - Database Schema: Mongodb compatible schema and query options
  - Schema expressed using typescript interfaces

## Libraries & Tools: 
- [React](https://reactjs.org/)
- [Nrwl Nx](https://nx.dev) : Scaffolding, build, lint, serve code
- [Grommet](https://v2.grommet.io/): UI Library 
- [Grommet Icons](https://icons.grommet.io/): Icons Library

- [is.js](https://is.js.org/)







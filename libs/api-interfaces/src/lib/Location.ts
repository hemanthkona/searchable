import { Address } from './Address';

export interface Location {
  location_string?: string;
  title?: string;
  address: Address;
}

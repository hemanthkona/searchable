export enum ConditionTitle {
  NEW = "New",
  USED = "Used",
  OTHER = "Other"
}
export interface Condition {
  _id: string;
  title: ConditionTitle;
  description: string;
}

import { Location } from './Location';
import { Image } from './Image';
import { User } from './User';
import { Condition } from './Condition';

export interface Product {
  _id: string;
  title: string;
  price: number;
  location?: Location;
  condition?: Condition["_id"];
  quantity_in_stock?: number;
  quantity_sold?: number;
  sku?: string;
  category?: string;
  icon?: string;
  image?: string;
  imageBase64?: string;
  images?: Image[],
  _created_user?: User["_id"];
  _last_updated_user?: User["_id"];
  _created_date?: Date;
  _last_updated_date?: Date;
}

import { Location } from './Location';

export interface QueryProduct {
  title?: string;
  condition?: Array<string>;
  price?: number;
  min_price?: number;
  max_price?: number;
  location_string?: string;
  location?: Location;
  category?: string;
  _created_date?: Date;
  _last_updated_date?: Date;
}

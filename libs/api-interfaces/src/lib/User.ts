import { Location } from "./Location";

export interface Name {
  first: string;
  last: string;
}

export interface User {
  _id: string;
  name: Name;
  email: string;
  location: Location;
  _created_date: Date;
  _last_updated: Date;
}

import { Product } from './Product';
import { QueryProduct } from './QueryProduct';

export interface Response {
  query?: QueryProduct;
  meta?: ResponseMeta;
  data: Product[]
}

interface ResponseMeta {
  count: number;
  total: number;
  page: number;
}

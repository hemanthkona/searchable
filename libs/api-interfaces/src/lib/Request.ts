import { QueryProduct } from './QueryProduct';

export interface Request {
  request_id: string;
  collection?: string;
  query: QueryProduct;
  options?: RequestOptions;
}

interface RequestOptions {
  sort: string;
  skip: number;
  limit: number;
  page: number;
  no_of_items_per_page: number;
}

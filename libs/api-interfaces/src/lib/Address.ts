export interface Address {
  street_number?: number;
  street?: string;
  street_extra?: string;
  zipcode?: string;
  province?: string;
  city?: string;
  country?: string;
}

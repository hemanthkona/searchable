export const countries = [
  {
    _id: "country001",
    title: "USA",
    description: "United States of America"
  },
  {
    _id: "country002",
    title: "Canada",
    description: "Canada"
  }
]


export const getCountryTitle = (id) => {
  let title = "";

  const foundItem = countries.find(elm => elm._id == id);
  if (foundItem && foundItem['title']) title = foundItem['title'];

  return title;
}

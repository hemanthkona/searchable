import { Condition, ConditionTitle  } from '@searchable/api-interfaces';

export const conditions: Condition[] = [
  {
    _id: "new001",
    title: ConditionTitle.NEW,
    description: "A brand new item"
  },
  {
    _id: "used001",
    title: ConditionTitle.USED,
    description: "Item has been used previously"
  },
  {
    _id: "other001",
    title: ConditionTitle.OTHER,
    description: "Other"
  }
]

export const getConditionTitle = (id) => {
  let title = "";

  const foundItem = conditions.find(elm => elm._id == id);
  if (foundItem && foundItem['title']) title = foundItem['title'];

  return title;
}

import * as is from 'is_js';

import { data } from './data';
import { Product, QueryProduct } from '@searchable/api-interfaces'
import { filterProducts } from '..';

export function filter(query: QueryProduct): Product[] {
  // console.log("query", query);
  // If search field is empty then return empty array
  if (is.undefined(query) || is.undefined(query.title) || query.title === '') return [];

  const result = data.filter((item) => {
    let found = false;

    if (item.title.toLowerCase().includes(query.title.toLowerCase())) {
      found = true;

      // condition
      if (found === true) {
        if (is.undefined(query.condition) || query.condition.length === 0) {
          found = true;

        } else if (query.condition.indexOf(item.condition) === -1) {
          found = false;
        }
      }

      // min_price
      if (found === true) {
        if (is.undefined(query.min_price) || is.nan(query.min_price)) {
          found = true;

        } else if (query.min_price * 100 > item.price) {
          found = false;
        }
      }

      // max_price
      if (found === true) {
        if (is.undefined(query.max_price) || is.nan(query.max_price)) {
          found = true;

        } else if (query.max_price * 100 < item.price) {
          found = false;
        }
      }

      // location.country
      if (found === true) {
        if (is.undefined(query.location) || is.undefined(query.location.address)
          || is.undefined(query.location.address.country) || query.location.address.country === '') {
            found = true;

        } else if (query.location.address.country !== item.location.address.country) {
          found = false;
        }
      }

      // location.city
      if (found === true) {
        if (is.undefined(query.location) || is.undefined(query.location.address)
          || is.undefined(query.location.address.city) || query.location.address.city === '') {
            found = true;

        } else if (query.location.address.city.toLowerCase() !== item.location.address.city.toLowerCase()) {
          found = false;
        }
      }
    }

    return found;
  });

  // console.log("Result", result);

  return result;
}

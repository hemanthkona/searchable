import React from 'react';
import {
  Box,
  Text,
} from 'grommet';
import { Icon } from './icon';
import { ProductsLoading } from './loading';

import { getConditionTitle, getCountryTitle } from '@searchable/data';

export const Products = ({ products, doneSeaching, isLoading }) => {
  return (
    <Box a11yTitle="List of products" width="70%" style={{'padding': '16px'}}>
      <h1>Products</h1>
      <hr/>
      { (doneSeaching && !isLoading ?
          ((products && products.length > 0) ? products.map((product, index) => {
            return (
              <Box className="product" a11yTitle="Produt details" direction="row" key={product._id} align="start">
                {/* <p> {product._id} </p> */}
                <Box className="product-icon">
                  <Icon src={product.icon} imageBase64={product.imageBase64}></Icon>
                </Box>
                <Box className="product-details" width="100%">
                  <h3> {product.title} </h3>
                  <p> Cad: $ { product.price / 100} </p>
                  <p> Location: {product.location.address.city}, {getCountryTitle(product.location.address.country)}  </p>
                  <p> Condition: {getConditionTitle(product.condition)} </p>
                </Box>
              </Box>
            )
          }) : <Text> No produts found </Text>)
          : <ProductsLoading></ProductsLoading>)
      }
    {/* End of products */}
    </Box>
  )

}

export default Products

import React from 'react';
import { BulletList } from 'react-content-loader';

export const ProductsLoading = () => {
  return (
    <BulletList
      backgroundColor={'#aaa'}
      foregroundColor={'#ccc'}
    />
  )
}

import React from 'react';
import {
  Box,
  Button,
  CheckBox,
  TextInput,
  Select
} from 'grommet';
import {
  Filter as FilterIcon
} from 'grommet-icons'

import * as is from 'is_js';

import { conditions, countries } from '@searchable/data';

export const Filters = ({queryProducts, onUpdateQueryProducts, onSearchProducts}) => {

  function handlePrice(type, event) {

    if (is.not.undefined(event) && is.not.undefined(event.target) && is.not.undefined(event.target.value)) {
      queryProducts[type] = parseInt(event.target.value);
    }

    onUpdateQueryProducts(type, queryProducts[type], event);
  }

  function handleConditions (event, id, state) {
    // console.log("handleConditions");
    const conditionIndex = queryProducts['condition'].indexOf(id);

    if (state) {
      if (conditionIndex === -1) queryProducts['condition'].push(id);

    } else {
      if (conditionIndex !== -1) {
        queryProducts['condition'].splice(conditionIndex, 1);
      }
    }

    onUpdateQueryProducts('condition', queryProducts['condition']);
  }

  function handleLocation(type, value) {
    queryProducts['location']['address'][type] = value;
    onUpdateQueryProducts('location', queryProducts['location']);
  }

  return (
    <Box a11yTitle="Filter products" width="30%" background={'#eee'} style={{'padding': '0 30px'}}>
      <Box direction="row" alignContent="between">
        <Box>
          <h2>Filter products:</h2>
        </Box>
        {/* <Box flex="grow"></Box> */}
        <Box alignSelf="center" className="search-button-container">
          <Button
            icon={<FilterIcon />}
            label="Filter"
            primary
            onClick={onSearchProducts}
          />
        </Box>
      </Box>
      {/* Price filter */}
      <h3>Price:</h3>
      <Box a11yTitle="Price filter" pad="xxsmall" direction="row" alignContent="between">
        <TextInput value={queryProducts.min_price} placeholder="min" title="min_price" onChange={(event) => handlePrice('min_price', event)}></TextInput>
        &nbsp;
        <TextInput value={queryProducts.max_price} placeholder="max" title="max_price" onChange={(event) => handlePrice('max_price', event)}></TextInput>
      </Box>

      {/* Condition filter */}
      <h3> Condition </h3>
      <Box a11yTitle="Filter by condition" pad="xxsmall">
        {conditions.map((condition) => {
          return (<CheckBox
            key={condition._id}
            checked={queryProducts.condition.indexOf(condition._id) !== -1}
            label={condition.title}
            onChange={(event) => handleConditions(event, condition._id, event.target.checked)}
            />)
          })}
      </Box>

      {/* Location filter */}
      <h3>Location:</h3>
      <Box a11yTitle="Filter by location" pad="xxsmall">
        <TextInput value={queryProducts.location.address.city} placeholder="City" title="city" onChange={(event) => handleLocation('city', event.target.value)}></TextInput>
        <br/>
        <Select
          options={countries}
          labelKey="title"
          valueKey="title"
          placeholder="Country"
          value={queryProducts.location.address.country}
          onChange={({ option }) => handleLocation('country', option._id)}>
        </Select>
      </Box>

    </Box>
  )
}

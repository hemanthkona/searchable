import React from 'react';
// const images = require.context('../../assets/images', true);
// import AndroidWatch from '../../assets/images/AndroidWatch.png';
// import AppleAirpods from '../../assets/images/AppleAirpods.png';
// import AppleIpad from '../../assets/images/AppleIpad.png';
// import AppleMacbook from '../../assets/images/AppleMacbook.png';
// import AppleTV from '../../assets/images/AppleTV.png';
// import AppleWatch from '../../assets/images/AppleWatch.png';
// import DellLaptop from '../../assets/images/DellLaptop.png';
// import DieselWatch from '../../assets/images/DieselWatch.png';
// import FossilWatch from '../../assets/images/FossilWatch.png';
// import HpLaptop from '../../assets/images/HpLaptop.png';
// import Insignia from '../../assets/images/Insignia.png';
// import Iphone from '../../assets/images/Iphone.png';
// import Iphone11 from '../../assets/images/Iphone11.png';
// import KateSpadeWatch from '../../assets/images/KateSpadeWatch.png';
// import LGLaptop from '../../assets/images/LGLaptop.png';
// import LGPhone from '../../assets/images/LGPhone.png';
// import LgTV from '../../assets/images/LgTV.png';
// import NexusPhone from '../../assets/images/NexusPhone.png';
// import SamsungAirpods from '../../assets/images/SamsungAirpods.png';
// import SamsungLaptop from '../../assets/images/SamsungLaptop.png';
// import SamsungPhone from '../../assets/images/SamsungPhone.png';
// import SamsungTv from '../../assets/images/SamsungTv.png';
// import SonyTV from '../../assets/images/SonyTV.png';
// import TimexWatch from '../../assets/images/TimexWatch.png';
// import VizioTV from '../../assets/images/VizioTV.png';



export const Icon = ({src, imageBase64}) => {
  // console.log(src);
  // let image = <img src={images('./' + src)} width="100" height="100" alt="icon" />;

  // if (imageBase64) {
  const image = <img src={imageBase64} width="100" height="100" alt="icon"/>
  // }

  return image;
}

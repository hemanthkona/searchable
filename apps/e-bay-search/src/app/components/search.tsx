import React from 'react';
import {
  Box,
  Button,
  TextInput
} from 'grommet';

import {
  Search as SearchIcon
} from 'grommet-icons';


export const Search = ({title, onUpdateQueryProducts, onSearchProducts}) => {
  return (
    <Box direction="row" pad="small" color={'light-3'}>
        <Box flex="grow">
          <TextInput value={title} placeholder="Search (Try: app or apple or watch)" onChange={event => onUpdateQueryProducts('title', event.target.value, event)}></TextInput>
        </Box>
        <Box className="search-button-container">
          <Button
            icon={<SearchIcon />}
            label="Search"
            primary
            onClick={onSearchProducts}
          />
        </Box>
      </Box>
  );
}

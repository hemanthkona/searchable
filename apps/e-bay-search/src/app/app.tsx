import React, { useEffect, useState } from 'react';
import {
  useHistory,
  useLocation
} from "react-router-dom";
import {
  Grommet,
  Box,
  Button,
  CheckBox,
  Header,
  Footer,
  Text,
  TextInput,
  Select
} from 'grommet';
import {
  Home,
  Search
} from 'grommet-icons';

import * as is from 'is_js';

// libs
import { Product, QueryProduct } from '@searchable/api-interfaces';
import { filterProducts, conditions, countries } from '@searchable/data';

// Components
import { Products } from './components/products';
import { Search as SearchComponent  } from './components/search';
import { Filters as FiltersComponent  } from './components/filters';

const theme = {
  button: {
    border: {
      radius: '10px'
    }
  }
}

// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const App = () => {
  const defaultQueryProducts: QueryProduct = {
    title: '',
    condition: [],
    location: {
      address: {
        city: '',
        country: ''
      }
    }
  };

  const [firstTime, setFirstTime] = useState(true);
  const [queryProducts, setQueryProducts] = useState<QueryProduct>(defaultQueryProducts);
  const [doneSeaching, setDoneSearching] = useState<boolean>(false);
  const [localQueryString, setLocalQueryString] = useState('');
  const [products, setProducts] = useState<Product[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  // const location = useLocation();
  // console.log(location);
  const queryParams = useQuery();
  const history = useHistory();

  useEffect(() => {
    let foundQueryParams = false;

    if (firstTime) {
      setFirstTime(false)

      // title
      if (is.not.undefined(queryParams.get('title')) && is.not.null(queryParams.get('title'))) {
        queryProducts['title'] = queryParams.get('title');
        foundQueryParams = true;
      }

      // min_price
      if (is.not.undefined(queryParams.get('min_price')) && is.not.null(queryParams.get('min_price'))) {
        queryProducts['min_price'] = parseInt(queryParams.get('min_price'));
        foundQueryParams = true;
      }

      // max_price
      if (is.not.undefined(queryParams.get('max_price')) && is.not.null(queryParams.get('max_price'))) {
        queryProducts['max_price'] = parseInt(queryParams.get('max_price'))
        foundQueryParams = true;
      }

      // conditions
      if (is.not.undefined(queryParams.get('condition')) && is.not.null(queryParams.get('condition'))) {
        queryProducts['condition'] = JSON.parse(queryParams.get('condition'));
        foundQueryParams = true;
      }

      // location
      if (is.not.undefined(queryParams.get('location')) && is.not.null(queryParams.get('location'))) {
        queryProducts['location'] = JSON.parse(queryParams.get('location'));
        foundQueryParams = true;
      }

      // set query params
    }

    if (foundQueryParams) {
      // set local query params
      const urlQueryParams = queryParams.toString();

      setLocalQueryString(urlQueryParams);
      // set query product obj
      setQueryProducts(queryProducts);
      // search products
      searchProducts();
    }
  }, []);

  function constructSearchString(field, value) {
    let removeField = false;
    if (queryParams.get(field)) {

      if (field === 'title') {
        if (is.undefined(value) || value === '') removeField = true;

      } else if (field === 'min_price' || field === 'max_price') {
        if (is.nan(value)) removeField = true;

      } else if (field === 'condition') {
        if (is.undefined(value) || is.empty(JSON.parse(value))) removeField = true;

      }
    }

    if (!removeField) {
      queryParams.set(field, value);

    } else {
      queryParams.delete(field)
    }
    setLocalQueryString(queryParams.toString());
  }

  function setUrlQueryParams() {
    history.push({
      pathName: '/',
      search: '?'+localQueryString
    });
  }

  function searchProducts() {
    console.log(localQueryString);
    // set url query params
    if (!firstTime) setUrlQueryParams();
    // set search done
    setDoneSearching(true);
    // set loading
    setIsLoading(true);

    // load results after 3 seconds
    setTimeout(() => {
      const filteredProducts: Product[] = filterProducts(queryProducts);
      setProducts(filteredProducts);
      // set loading to false
      setIsLoading(false);

    }, 2000);
  }

  function handleQueryProducts(field, value, event) {
    queryProducts[field] = value;

    if (is.object(value)) {
      constructSearchString(field, JSON.stringify(value));

    } else {
      constructSearchString(field, value);
    }

    setQueryProducts(queryProducts);
  }

  return (
    <Grommet theme={theme}>

      <Header background="brand" gap="none">
        <Button size='small' icon={<Home />} hoverIndicator />
        <h1> eBay Search </h1>
      </Header>

      {/* Search Box */}
      <SearchComponent title={queryProducts.title} onSearchProducts={searchProducts} onUpdateQueryProducts={handleQueryProducts}></SearchComponent>
      {
        (doneSeaching)?
          (<Box alignContent="between" direction="row">
            {/* Filters */}
            <FiltersComponent queryProducts={queryProducts} onSearchProducts={searchProducts} onUpdateQueryProducts={handleQueryProducts}></FiltersComponent>
            {/* products list   */}
            <Products products={products} doneSeaching={doneSeaching} isLoading={isLoading}></Products>
          </Box>): (<Text> </Text>)
      }

      <Footer background="brand" pad="medium">

      </Footer>
    </Grommet>
  );
};

export default App;
